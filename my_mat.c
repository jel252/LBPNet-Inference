/*****************************
 * Source of my math library *
 * By Jeng-Hau Lin           *
 *****************************/

#include "my_mat.h"

int shReLU(int x, int th){
    if(x>=th)
        return x;//-th;
    else
        return th;//-th;
}

//int checkMax(float * output, int nc, int ans_byte){
int checkMax(short * output, int nc, int ans_byte){

    short outMaxV = output[ans_byte];
    int i;
    for(i=0;i<nc;++i){
        if(i==ans_byte)
            continue;

        if(output[i]>outMaxV)//+10)
            return 0;
    }

    return 1;
}

int prtArr(float arr[], const size_t n){
    int i;
    for(i=0;i!=n;++i){
        printf("%10f,", arr[i]);
    }
    printf("\n");

    return 0;
}

int prtArrInt(int arr[], const size_t n){
    int i;
    for(i=0;i!=n;++i){
        printf("%8d,", arr[i]);
    }
    printf("\n");

    return 0;
}

int prtArrCha(u_char arr[], const size_t n){
    int i;
    for(i=0;i!=n;++i){
        printf("%8d,", arr[i]);
    }
    printf("\n");

    return 0;
}

int prtArrCha2(char arr[], const size_t n){
    int i;
    for(i=0;i!=n;++i){
        printf("%8d,", arr[i]);
    }
    printf("\n");

    return 0;
}

int prtArrSho(short arr[], const size_t n){
    int i;
    for(i=0;i!=n;++i){
        printf("%8d,", arr[i]);
    }
    printf("\n");

    return 0;
}

int prtMtx(float *mtx, const size_t n_r, const size_t n_c){
    int r, c;
    for(r=0;r!=n_r;++r){
        for(c=0;c!=n_c;++c){
            printf("%10f,", *(mtx + r*n_c + c));
            //printf("%3d", (int)*(mtx + r*n_c + c));
        }
        printf("\n");
    }
    return 0;
}

int prtMtxInt(int *mtx, const size_t n_r, const size_t n_c){
    int r, c;
    for(r=0;r!=n_r;++r){
        for(c=0;c!=n_c;++c){
            printf("%2d", *(mtx + r*n_c + c));
        }
        printf("\n");
    }
    return 0;
}

int prtMtxF2I(float *mtx, const size_t n_r, const size_t n_c){
    int r, c;
    for(r=0;r!=n_r;++r){
        for(c=0;c!=n_c;++c){
            printf("%2d", (int)(round(*(mtx + r*n_c + c))));
        }
        printf("\n");
    }
    return 0;
}
