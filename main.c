/*****************************
 * 2018-04-17                *
 * Test a Neural Network     *
 * plan implementation       *
 * read weights into memory  *
 * 5-layer:                  *
 *         1 LBP layer       *
 *         2 LBP layer       *
 *         3 LBP layer       *
 *         4 FC layer        *
 *         5 FC layer        *
 * By Jang-Hau Lin           *
 *****************************/

#include <assert.h>
#include "MLP_5Ls.h"
#define Ntrains 100

//extern unsigned _stklen = 100000000U;
// batch normalization + Adamax
int main(int argc, char *argv[]){
    //srand(time(NULL)*getpid());

	u_int32_t ilen = 28*28;
    // LBP layer kernels and random projection maps
    u_char p_Kx1[N_1*4]={0}; u_char p_Ky1[N_1*4]={0}; u_char p_RP1[N_1*4]={0};
    u_char p_Kx2[N_2*4]={0}; u_char p_Ky2[N_2*4]={0}; u_char p_RP2[N_2*4]={0};
    u_char p_Kx3[N_3*4]={0}; u_char p_Ky3[N_3*4]={0}; u_char p_RP3[N_3*4]={0};
    // FC layer weight
    char p_w4[N_4*(N_3+N_2+N_1+N_0)*6*6] = {0};
    char p_w5[N_5*N_4] = {0};
    // FC layer Bias
    char p_b4[N_4]={0}; char p_b5[N_5]={0};
    // Batch normalization
    char p_BNw[N_4]={0}; short p_BNb[N_4]={0};
    // Record the weigth sum before activation function 
    u_char image[ilen];
    u_char pA0[N_0*36*36]={0};
    u_char pA3[(N_3+N_2+N_1)*36*36]={0}; 
    short pA3_avp[(N_3+N_2+N_1+N_0)*6*6]={0};
    short pA4[N_4]={0};                       
    short pA4_bn [N_4]={0};
    short pA5[N_5]={0};
   
    NNet my_NNet2 = {
        p_Kx1, // the Kxeights betKxeen layer 0 and 1 
        p_Ky1, // the Kyeights betKyeen layer 0 and 1 
        p_Kx2, // the Kxeights betKxeen layer 1 and 2
        p_Ky2, // the Kyeights betKyeen layer 1 and 2
        p_Kx3, // the Kxeights betKxeen layer 2 and 3
        p_Ky3, // the Kyeights betKyeen layer 2 and 3

        p_RP1,
        p_RP2,
        p_RP3,

        p_w4, // the weights between layer 3 and 4
        p_b4, // the beights betbeen layer 3 and 4
        p_w5, // the weights between layer 4 and 5
        p_b5, // the beights betbeen layer 4 and 5

        p_BNw, // Scaline
        p_BNb, // Shifting
    };

    // Some pre-trained data for testing
    readWeightsLBP("LBP_param/lbp_w_b_com_mF_Q_8_Q.bin", &my_NNet2);

    const char * teLfile = "../../Cpp/LBPNet_20180413/MNIST/t10k-labels-idx1-ubyte";
    const char * teIfile = "../../Cpp/LBPNet_20180413/MNIST/t10k-images-idx3-ubyte";
    
    int fl, fi;
    fl = open(teLfile, O_RDONLY, 0);
    fi = open(teIfile, O_RDONLY, 0);

    // Count errors
    int errCnt = 0;

    time_t timer1, timer2;
    double seconds;
    time(&timer1);
    int i;
    int c = 0;
	u_char byte = 0;
    printf("Start to test!\n");
    errCnt = 0;
    int w = 32;
        
    for(i=0; i<N_SAMPLES; ++i){
        // Forward propogation
        lseek(fi, 16 + i*ilen, SEEK_SET);
        lseek(fl, 8 + i, SEEK_SET);
        c = read(fi, image, ilen);
        c = read(fl, &byte, sizeof(byte));

        forwardIn(image, pA0, 28); // per-processing image

        //printf("Start the 1st conv layer!\n");
        forwardLBP(pA0, pA3, my_NNet2.pKx1, my_NNet2.pKy1, my_NNet2.pRP1, N_0, N_1, w);

        //printf("Start the 2nd conv layer!\n");
        forwardLBP(pA0, pA3, my_NNet2.pKx2, my_NNet2.pKy2, my_NNet2.pRP2, N_1+N_0, N_2, w);

        //printf("Start the 3rd conv layer!\n");
        forwardLBP(pA0, pA3, my_NNet2.pKx3, my_NNet2.pKy3, my_NNet2.pRP3, N_2+N_1+N_0, N_3, w);

        //printf("Start the average pooling layer!\n");
        forwardAVP(pA3, pA0, pA3_avp, N_3+N_2+N_1+N_0, 32, 5);

        //printf("Start the 1st linear layer!\n");
        forwardFC2(pA3_avp, my_NNet2.pW4, my_NNet2.pb4, pA4, (N_3+N_2+N_1+N_0)*6*6, N_4, 10);

        //printf("Start the batch normalization layer!\n");
        forwardBN(pA4, my_NNet2.pBNw, my_NNet2.pBNb, pA4_bn, N_4, 4);

        //printf("Start the 2nd linear layer!\n");
        forwardFC2(pA4_bn, my_NNet2.pW5, my_NNet2.pb5, pA5, N_4, N_5, 6);

        if(!checkMax(pA5, N_5, byte)){
            ++errCnt;
        }
    }

    close(fi);
    close(fl);

    time(&timer2);
    seconds = difftime(timer2,timer1);

    printf("\n");
    int a,b;
    for(a=0;a<28;++a){
        for(b=0;b<28;++b){
            printf("%3d", *(image + a*28 +b));
        }
        printf("\n");
    }

    printf("\n");
    printf("the last target: %d\n", byte);
    printf("the last output:\n");
    prtArrSho(pA5, N_5);
    printf("weights:\n");
    prtArrCha2(my_NNet2.pW5, 10);
    printf("Time elapsed: %d\n", (int)seconds);
    printf("error = %f%%\n", (float)errCnt/((float)N_SAMPLES)*100.0);

    //free(p_w4);
    return 0;
}

