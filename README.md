# LBPNet-Inference

Memory and computation efficient deep learning architectures are crucial to continued proliferation of machine learning capabilities to new platforms and systems.
Binarization of operations in convolutional neural networks has shown promising results in reducing model size and increasing computing efficiency. 
In this paper, we tackle the problem using a strategy different from the existing literature by proposing local binary pattern networks or LBPNet, that is able to 
learn and perform binary operations in an end-to-end fashion.
LBPNet uses local binary comparisons and random projection in place of conventional convolution (or approximation of convolution) operations. 
These operations can be implemented efficiently on different platforms including direct hardware implementation. 
We applied LBPNet and its variants on standard benchmarks. The results are promising across benchmarks while providing an important means to improve memory and 
speed efficiency that is particularly suited for small footprint devices and hardware accelerators.

In this repository, we briefly introduce the theory and training algorithm of LBPNet, and then move on the the implementation on FPGA. 
The provided C code only include the inference of a LBPNet for MNIST dataset because we are not allowed to release the CUDA training code for LBPNet besed on a NDA from our supporter.

# Theory
LBPNet is designed based on one of the mainstreams in computer vision called "Local Binary Pattern". LBP operation includes simply comparison and memory allocation. 
However, all previous works in the literature did not explore the full capability of LBP, i.e., they all limit the sampling patterns to be circles. 
We apply optical flow theory and convex optimization methds to propose LBPNet, which can deform the sampling patterns to extract useful features from images and,
thereby, improve the classification.

<br><img src="https://gitlab.com/jel252/LBPNet-inference/raw/master/Img/overview.png"  width="40%" ><br>
An illustration of the LBPNet architecture. The LBP operation is composed of comparison and bit-allocation, while the channel fusion is done by random projection.


<br><img src="https://gitlab.com/jel252/LBPNet-inference/raw/master/Img/LBP.png"  width="40%" ><br>
The sub-figure(a) above illustrate the previous use of LBP pattern, in which all sampling points are arranged on a circle. In this toy example, there are 8 sampling 
points shown as green round dots and 1 pivot sampling point shown as shaded red star in the middle of a LBP pattern. All the 8 sampled pixel strengths are compared
with the center pivot sampled strength. Then, the compared results, which are 8 booleans, are allocated to a binary array to form an integer for next step of 
computer vision application. In the literautre, the sampling patterns are pre-defined, the only variation is the radii of the patterns.
On the other hand, we discover that with a proper deformation of the sampling patterns as shown in sub-figure (b) to (d), the classification accuracy can be improved. 

In the next 2 sub-sections, we explain the forward and backward propagation of LBPNet.

## Design of the Forward Propagation
<br><img src="https://gitlab.com/jel252/LBPNet-inference/raw/master/Img/LBP_inP2_hor.png"  width="40%" ><br>
Supposed we got an image with 2 channels, the LBP pattern slides over the images and keeps sampling pixel strengths according to the positions in a widow and 
generate binary arrays as shown in the left bottom and right botton for the two channels.


<br><img src="https://gitlab.com/jel252/LBPNet-inference/raw/master/Img/LBP_inP2_rndPrj.png"  width="40%" ><br>
To prevent channel explosion, CNN directly sums all results from channels, which is equivalent to a 1x1 convolution after 2 dimensional convolutions. However, we
resort to random projection to avoid adders. The projection maps are pre-defined before training, and fixed throughout the learning process. (We are working on 
the learning of projection maps.) In other words, the sampling patterns learn to extract useful features subject to the fixed projection maps.


## Backward Propagation
There are mainly two problems. The first one is that the comparison is not differentiable, and the other problem is that previous works incooperate on other heavily
computational techniques to make LBP useful. If we develop an algorithm with only LBP operation and random projection, there is no guarantee that the learning of 
LBP patterns can extract meanful features for image classification.


<br><img src="https://gitlab.com/jel252/LBPNet-inference/raw/master/Img/approx.png"  width="35%" ><br>
The first problem is easy to solve since BNN already demonstrate the effectness of the approximation from non-differentiable function to a smooth differentiable one.
Here, we approximate the comparison function shown in red with a scaled and shifted hyperbolic tengent function shown in blue on the left subfigure. 


<br><img src="https://gitlab.com/jel252/LBPNet-inference/raw/master/Img/opticalFlow.png"  width="55%" ><br>
The second problem is the fatal one stopping people from making LBPNet work. Nevertheless, the effectiveness of LBPNet is true based on our implementation and results.
The answer is the optical flow theory. Optical flow theory build a link between image gradient and the pixel strength variation across time. 
If we apply calculus chain rule on the loss of LBPNet toward every position of the sampling points, the last term will be image gradient.
For more details, please refer to our article in the reference section.
In summary, if we deform the sampling pattern, we can accumulate different image gradients. Pushing those sampling points along with image gradients will reduce the
difference of the output feature maps, and then extract common features among images.

# Network Structure
<br><img src="https://gitlab.com/jel252/LBPNet-inference/raw/master/Img/ResNet-like.png"  width="40%" ><br>
LBPNet is a strong edge detector because of the nature of comparing operation, but it experiences a hard time on preserving low (spatial) frequency components of the 
image.
To compensate this weakness, we introduce a shortcut branch in each LBP block. 
The sub-figure (a) shows the basic building block a a residue network, which contains a shortcut path from the input node to the output node. 
The shortcut branch and the convolution-relu branch are summed together to learn the residues of the output feature maps.
In our design as shown in sub-figure (b), we adopt also a shortcut path mainly to preserve low frequency components.
The sub-figure (b) is simply a transition in our exploration because our ultimate goal is to exclude all multiplication-and-accumulation (MAC) operations in a LBP block.
Then, we propose the LBP block shown in sub-figure (c), which is totally MAC-free.
We use random projection to fuse channels, and pass the output feature maps to a shifted ReLU, which is a ReLU function with a threshold other than 0 because LBP's 
output is non-negative.
The shifted-ReLU is designed to introduce non-linearity into LBPNet, if the LBP's output has n bits, the shifted-ReLU outputs zero if its input is smaller than 0.5*(2^n -1)
The last node in the sub-figure (c) is to join the shortcut branch and the shifted-ReLU together by stack the channels together.

# Experimental results
<br><img src="https://gitlab.com/jel252/LBPNet-inference/raw/master/Img/LBPaccuracy.png"  width="35%" ><br>
The experimental results are shown in the table above.
We implement two versions of LBPNets, which are composed by the transition block with 1-by-1 convolution for the channel fusion in the sub-figure 
(b) and the block wiht random projection in the sub-figure (c) in previous section. 
LBPNet(RDP) achieves the state-of-the-art results on MNIST and SVHN, but the results on CIFAR-10 are not good enough.
As the aforementioned, because LBPNet is a strong edge detector, its perfomance on MNIST and SVHN are good because these 2 datasets' have distinguishable outlines
in the image contents.
CIFAR-10 is composed by real objects in daily life, the outlines of objects are usually obsecure or contain tool many gradual transitions that confuse LBPNet.
We are working on introducing more stochastic techinques to compensate the weakness while maintain LBPNet to be hardware friendly.

# FPGA Hardware Accelerator for LBPNet on MNIST
<section>
    <img src="https://gitlab.com/jel252/LBPNet-inference/raw/master/Img/subject.png"  width="30%" >
    <img src="https://gitlab.com/jel252/LBPNet-inference/raw/master/Img/subject_tab.png"  width="30%" >
</section>
We move on the implement a hardware accelerator for LBPNet on MNIST.
The figure and table show the structure, paramenter counts and memory footprints of the LBPNet achiving 99.50% accuracy on MNIST.
As you can see, the LBPLayers requires less than 10Kb to store all parameters because all we need to memorize is the sparse and discrete locations of the sampling points
and the random projection map.
There is no compression technique adopted for the LBPLayers.
With this tiny memory requirement, we can pre-load all LBP parameters on chip as close to the computing units as possible to eliminate buffering.
The speed and memory bottleneck of this hardware comes from the FCLayers.
During training, the FCLayers are still in floating numbers.
In this repository, we have quantize all FCLayers to 16-bit short integers or 8-bit characters to obtain the 29Mb of the FC parameters.
(To break this bottleneck, we have combined LBP and binarized FCLayers and got 99.37% accuracy on MNIST.)

This repository provides a synthesizable C code together with the trained parameters include the LBPLayers and FCLayers.

# FPGA architecture
<br><img src="https://gitlab.com/jel252/LBPNet-inference/raw/master/Img/HA_LBPNet_arch.png"  width="35%" ><br>
The streamline architecture we use is shown above.
Because we stack up the output channels of LBP and shortcut branches, the data buffer is engineered to make use of the stacking and avoid unnecessary buffering.
In brief, we combine input and output into a single data buffer and build the buffer from bottom to top.
The other buffer is designed for the FCLayers because they are usually too large to store on-chip.
It is worth to mention that even BNN with 8MB FC parameters cannot store them on-chip.
The current feasible solution is to store FC parameters off-chip and buffer them when necessary.

# Evaluation of the Hardware Accelerator
<br><img src="https://gitlab.com/jel252/LBPNet-inference/raw/master/Img/resource.png"  width="35%" ><br>
We implemented our design in synthesible C and used Xilinx Vivado HLS and Vivado Suite 2015.4 as the primary tool for synthesizing our accelerator.
We evaluate our design on a low-cost Xilinx Zynq-7000 series (XC7Z020 FPGA). 
This FPGA contains 280 BRAM, 220 DSP, 13055 LUT, and 8148 FF.
A decent design space exploration to select the good design options is performed so that we get a design that has a good balance between
resource utilization and latency. It should be noted that the LBP compute unit is made up of 4 columns and 3 rows of PEs.
We compare our design with off-the-shelf CNN and BNN FPGA implementations. 
The table above compares the resource utilization for different FPGA implementations of LeNet and BNN with LBPNet. 
As can be seen in this figure, our accelerator is relatively more efficient. 
It utilizes only 40% of BRAMs, 51% of DSP units, 67% of LUTs, and 39% of Flip flops on our target FPGA.
Comparing to CNN architectures, we mostly have better resource utilization, execution time,and power consumption.

# Energy Efficiency
<br><img src="https://gitlab.com/jel252/LBPNet-inference/raw/master/Img/energy.png"  width="35%" ><br>
The table compares the speed and energy efficiencies of BNN, GPU and LBPNet. 
LBPNet have a better power consumption and latency when compared to CNN works. 
For example, BNN utilizes 3.32 W power, while our accelerator consumes only 1.92 W to perform classification. 
Comparing to BNN our resource utilization is lower, and we have relatively lower execution time. 
Our accelerator performs the classification of one image in 5.2 ms while the BNN work reported their execution time as 6.36 ms. 
In general, we could achieve a good balance between resource utilization and latency using LBPNet.
Comparing with with LBPNet’s inference on a CUDA-supported GPU, of which the latency is 0.7 ms per image, 
the average power consumption is 23.5W, and the memory consumption is 570 MByte, our FPGA implementation 
is 6.7X slower than a Tesla K40 GPU, but 12.3X more power efficient over GPU.

# Reference
Lin, Jeng-Hau, Yunfan Yang, Rajesh Gupta, and Zhuowen Tu. "Local Binary Pattern Networks." arXiv preprint arXiv:1803.07125 (2018).

