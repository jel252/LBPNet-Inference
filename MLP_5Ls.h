/************************************
 * Header of Multi-layer Perceptron *
 * save weights in memory        *
 * 5 layers:                        *
 *          1 input layer           *
 *          2 hidden layer          *
 *          3 hidden layer          *
 *          4 hidden layer          *
 *          5 output layer          *
 * By Jeng-Hau Lin                  *
 ************************************/

#ifndef __MLP_5Ls__
#define __MLP_5Ls__

#include "my_mat.h"
#define N_SAMPLES 10000 // The number of samples

#define N_0 1 // The number of nodes in the input layer
#define N_1 39 // The number of nodes in the 1st layer
#define N_2 40 // The number of nodes in the hidden layer
#define N_3 80 // The number of nodes in the hidden layer
#define N_4 512 // The number of nodes in the hidden layer
#define N_5 10 // The number of nodes in the 5th layer

//=========== Use shifted ReLU ===========
#define gL(x) shReLU(x, 7)
#define g(x) shReLU(x, 0)


typedef struct NeuNet{
    u_char * pKx1; // the weights between layer 0 and 1
    u_char * pKy1; // the weights between layer 0 and 1
    u_char * pKx2; // the weights between layer 1 and 2
    u_char * pKy2; // the weights between layer 1 and 2
    u_char * pKx3; // the weights between layer 2 and 3
    u_char * pKy3; // the weights between layer 2 and 3

    u_char * pRP1; // the random projection map 1
    u_char * pRP2; // the random projection map 1
    u_char * pRP3; // the random projection map 1

    char * pW4; // the weights between layer 3 and 4
    char * pb4; // the weights between layer 3 and 4
    char * pW5; // the weights between layer 4 and 5
    char * pb5; // the weights between layer 4 and 5

    char * pBNw; // Scaline
    short * pBNb; // Shifting
} NNet;

int forwardIn(u_char * input, u_char * pA0, int w);

int forwardFC2(short * pAi, char * pWj, char * pbj, short * pAj, int ni, int nj, int scale_bit);
int forwardLBP(u_char * pAi, u_char * pAj, u_char * pKxij, u_char * pKyij, u_char * pRPj, int ni, int nj, int w);
int forwardAVP(u_char * pAi, u_char * pAk, short * pAj, int ni, int w, int k);
int forwardBN(short * pAi, char * pBNw, short * pBNb, short * pAj, int ni, int scale_bit);

int readWeightsLBP(const char str[], NNet *pNet);

#endif
