/*****************************
 * Header of my math library *
 * By Jeng-Hau Lin           *
 *****************************/

#ifndef __MY_MAT__
#define __MY_MAT__

#include <math.h>
#include <stdlib.h>
#include <stdio.h> 
#include <fcntl.h>// for file cotrol to access dataset
#include <omp.h>  // for OpenMP multi-threading of loops
#include <time.h> // for random seed
#include <unistd.h>

#define max(a,b) \
    ({ __typeof__ (a) _a = (a); \
     __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })

int shReLU(int x, int th);

int checkMax(short * output, int nc, int ans_byte);

int prtArr(float arr[], const size_t n);
int prtArrInt(int arr[], const size_t n);
int prtArrCha(u_char arr[], const size_t n);
int prtArrCha2(char arr[], const size_t n);
int prtArrSho(short arr[], const size_t n);
int prtMtx(float *mtx, const size_t n_r, const size_t n_c);
int prtMtxInt(int *mtx, const size_t n_r, const size_t n_c);
int prtMtxF2I(float *mtx, const size_t n_r, const size_t n_c);

#endif

