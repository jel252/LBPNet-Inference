/************************************
 * Source of Multi-layer Perceptron *
 * save weightings in memory        *
 * 5 layers:                        *
 *          1 input layer           *
 *          2 hidden layer          *
 *          3 hidden layer          *
 *          4 hidden layer          *
 *          5 output layer          *
 * By Jeng-Hau Lin                  *
 ************************************/

#include "MLP_5Ls.h"
#include <assert.h>

int forwardIn(u_char * input, u_char * pA0, int w){
    int i,j; // i for 1st and j for 2nd
    int k = 9;
    int h = floor(k/2.0);
    int w2 = w+2*h;

    for(j=0;j<w;++j){
        for(i=0;i<w;++i){
            pA0[(j+h)*w2 + (i+h)] = input[j*w + i];
            //pA0[159*w2*w2 + (j+h)*w2 + (i+h)] = input[j*w + i];
        }
    }
    return 0;
}

int forwardFC2(short * pAi, char * pWij, char * pbj, short * pAj, int ni, int nj, int scale_bit){
    int i,j; // i for 1st and j for 2nd
    for(j=0;j<nj;++j){
        int accumul = (int)pbj[j];
        for(i=0;i<ni;++i){
                accumul += (int)pAi[i] * (int)pWij[j*ni+i];
        }
        pAj[j] = accumul >> scale_bit; // Activation function
    }
    return 0;
}


int forwardLBP(u_char * pAi, u_char * pAj, u_char * pKxij, u_char * pKyij, u_char * pRPj, int ni, int nj, int w){
    int i,j; // i for 1st and j for 2nd
    int x,y; // x for horizontal and y for vertical of the output image
    int a; // a for hori in a filter and b for vert in a filter
    int k = 5;
    int kk = k*k;
    int h = (u_char)floor(k/2);
    int w2 = w + 2*h;
    int w2w2 = w2*w2;
    int ww = w*w;
    int n_samples = 4;
    int ns = 160 - ni - nj;
    int nsh = 160 - ni;

    for(y=0;y<w;++y){ // iterate over vertical axis
        for(x=0;x<w;++x){ // iterate over horizontal axis
            for(j=0;j<nj;++j){ // iterate over the output channels
                pAj[(j+ns)*w2w2 + (y+h)*w2 + (x+h)] = 0;
                for(a=0; a<n_samples; ++a){
                    u_char p  =  pRPj[j*n_samples + a]; // Look up the random projection table
                    u_char kX = pKxij[j*n_samples + a];
                    u_char kY = pKyij[j*n_samples + a];

                    if(p < ni-1){ 
                        // This pair of cPx and px shoud be of int<4>
                        u_char cPx = pAj[(p+nsh)*w2w2 + (y+h)*w2 + (x+h)];
                        u_char px = pAj[(p+nsh)*w2w2 + (y+kY)*w2 + (x+kX)];

                        if (px > cPx){
                            pAj[(j+ns)*w2w2 + (y+h)*w2 + (x+h)] |= 0x1 << a;
                        }
                    }else{ // Read from the image of dataset in u_char
                        u_char cPx = pAi[(y+h)*w2 + (x+h)];
                        u_char px  = pAi[(y+kY)*w2 + (x+kX)];
                        if (px > cPx){
                            pAj[(j+ns)*w2w2 + (y+h)*w2 + (x+h)] |= 0x1 << a;
                        }
                    }
                }
                pAj[(j+ns)*w2w2 + (y+h)*w2 + (x+h)] = gL(pAj[(j+ns)*w2w2 + (y+h)*w2 + (x+h)]);
            }
        }
    }

    return 0;
}

int forwardAVP(u_char * pAi, u_char * pAk, short * pAj, int ni, int w, int k){
    int x,y; // x for horizontal and y for vertical of the output image
    int i; // i for 1st and j for 2nd
    int a,b; // a for hori in a filter and b for vert in a filter
    int kk = k*k;
    int h = (int)floor(k/2);
    int r = (int)floor(w/k);
    int rr = r*r;
    int w2 = w + 2*h;
    int w2w2 = w2*w2;

    for(y=0;y< r;++y){ // iterate over vertical axis
        for(x=0;x< r;++x){ // iterate over horizontal axis
            for(i=0;i<ni-1;++i){ // iterate over the input channels
                int accumul = 0.0;
                for(b=0;b<k;++b){
                    for(a=0;a<k;++a){
                        accumul += (int)pAi[i*w2w2 + (y*k+b+h)*w2 + x*k+a+h]; 
                    }
                }
                accumul -= 7 * kk;

                pAj[i*rr + y*r + x] = accumul;
            }
            
            int accumuli = 0.0;
            for(b=0;b<k;++b){
                for(a=0;a<k;++a){
                    accumuli += (int)pAk[(y*k+b+h)*w2 + x*k+a+h]; 
                }
            }
            pAj[(ni-1)*rr + y*r + x] = (int)((float)accumuli /17.0);
            
        }
    }
    return 0;
}

int forwardBN(short * pAi, char * pBNw, short * pBNb, short * pAj, int ni, int scale_bit){
    int i; // i for 1st and j for 2nd
    for(i=0;i<ni;++i){
        pAj[i] = g((int)pAi[i] * (int)pBNw[i] + (short)pBNb[i]) >> scale_bit;
    }
    return 0;
}


int readWeightsLBP(const char str[], NNet *pNet){

    FILE *fp = fopen(str, "r");

    int c;
    c = fread(pNet->pKx1, sizeof(u_char), N_1*4, fp);
    c = fread(pNet->pKy1, sizeof(u_char), N_1*4, fp);
    printf("Kx1:\n"); prtArrCha(pNet->pKx1, 4);
    printf("Ky1:\n"); prtArrCha(pNet->pKy1, 4);
    c = fread(pNet->pKx2, sizeof(u_char), N_2*4, fp);
    c = fread(pNet->pKy2, sizeof(u_char), N_2*4, fp);
    printf("Kx2:\n"); prtArrCha(pNet->pKx2, 4);
    printf("Ky2:\n"); prtArrCha(pNet->pKy2, 4);
    c = fread(pNet->pKx3, sizeof(u_char), N_3*4, fp);
    c = fread(pNet->pKy3, sizeof(u_char), N_3*4, fp);
    printf("Kx3:\n"); prtArrCha(pNet->pKx3, 4);
    printf("Ky3:\n"); prtArrCha(pNet->pKy3, 4);

    c = fread(pNet->pRP1, sizeof(u_char), N_1*4, fp);
    printf("RP1:\n"); prtArrCha(pNet->pRP1, 4);
    c = fread(pNet->pRP2, sizeof(u_char), N_2*4, fp);
    printf("RP2:\n"); prtArrCha(pNet->pRP2, 4);
    c = fread(pNet->pRP3, sizeof(u_char), N_3*4, fp);
    printf("RP3:\n"); prtArrCha(pNet->pRP3, 4);

    c = fread(pNet->pW4, sizeof(char), N_4*(N_3+N_2+N_1+N_0)*36, fp);
    c = fread(pNet->pb4, sizeof(char), N_4, fp);
    c = fread(pNet->pW5, sizeof(char), N_5*N_4, fp);
    c = fread(pNet->pb5, sizeof(char), N_5, fp);
    printf("W4:\n"); prtArrCha2(pNet->pW4, 9);
    printf("b4:\n"); prtArrCha2(pNet->pb4, 9);
    printf("W5:\n"); prtArrCha2(pNet->pW5, 9);
    printf("b5:\n"); prtArrCha2(pNet->pb5, 9);

    c = fread(pNet->pBNw, sizeof(char), N_4, fp);
    c = fread(pNet->pBNb, sizeof(short), N_4, fp);
    printf("BN w:\n"); prtArrCha2(pNet->pBNw, 9);
    printf("BN b:\n"); prtArrSho(pNet->pBNb, 9);

    fclose(fp);
    return 0;
}

